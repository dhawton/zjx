<?php
/****************************************************
 * Copyright 2014 Daniel A. Hawton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ATCController
{
    public static function getStaff()
    {
        global $app,$data;

        $data['atm'] = $app->db->query('select')->table('roster')->where('staffrole', 'atm')->execute()->asArray();
        $data['datm']= $app->db->select()->from('roster')->where('staffrole', 'datm')->execute()->asArray();
        $data['ta'] = $app->db->select()->from('roster')->where('staffrole', 'ta')->execute()->asArray();
        $data['ata'] = $app->db->select()->from('roster')->where('staffrole', 'ata')->execute()->asArray();
        $data['ec'] = $app->db->select()->from('roster')->where('staffrole', 'ec')->execute()->asArray();
        $data['aec'] = $app->db->select()->from('roster')->where('staffrole', 'aec')->execute()->asArray();
        $data['fe'] = $app->db->select()->from('roster')->where('staffrole', 'fe')->execute()->asArray();
        $data['wm'] = $app->db->select()->from('roster')->where('staffrole', 'wm')->execute()->asArray();
        $data['ins'] = $app->db->select()->from('roster')->where('tstaff', 2)->execute()->asArray();
        $data['mtr'] = $app->db->select()->from('roster')->where('tstaff', 1)->execute()->asArray();

        return \Lyanna\View\View::make();
    }

    public static function getTest()
    {
        if (\ZJX\Auth\Auth::inGroup(\ZJX\Forum\Groups::find("WM")))
            echo "Yes";
        else
            echo "No";
    }

    public static function getOnline()
    {
        global $app;

        $json = array(); $x=0;
        $data = $app->db->select()->from("atc_online")->join('roster', array('roster.id','atc_online.cid'), 'left')->execute();

        foreach ($data as $ctrl) {
            $json[$x]['position'] = $ctrl->atc;
            $json[$x]['frequency'] = $ctrl->freq;
            $json[$x]['controller'] = $ctrl->lastname . ", " . $ctrl->firstname;
            $x++;
        }

        echo json_encode($json);
    }
} 