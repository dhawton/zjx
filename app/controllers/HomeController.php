<?php
use \Lyanna\View\View;

class HomeController
{
    public static function getIndex()
    {
        global $app, $data;

        $data['feed'] = $app->db->get()->execute("SELECT `smf_topics`.`id_topic`,`smf_messages`.`poster_time`,`smf_messages`.`subject` FROM "
            . "`smf_messages`,`smf_topics` WHERE `smf_topics`.`id_board`='2' AND `smf_topics`.`id_first_msg`=`smf_messages`.`id_msg` ORDER BY "
            . "`smf_messages`.`poster_time` DESC LIMIT 5")->asArray();

        return View::make('Home/Index');
    }
}