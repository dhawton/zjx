<?php
/****************************************************
 * Copyright 2014 Daniel A. Hawton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Lyanna\Config;
use Lyanna\Foundation\Exception;
use Lyanna\Foundation\Request;
use ZJX\Auth\Auth;
use ZJX\Forum\Groups;

class FilesController
{
    public static function getIndex($params = null)
    {
        global $app;
        $json = array();
        $data = array();

        if (preg_match("#^\d+$#", $params['id'])) {
            static::checkAndRender($params['id'], false);
            return;
        }

        if (!Auth::inGroup(array(Groups::find("WM"), Groups::find("EC"), Groups::find("AEC"), Groups::find("TA"), Groups::find("ATA"), Groups::find("ATM"), Groups::find("DATM"))))
            Exception::throwHttpError(500);

        if (!isset($params['id']) || $params == null) {
            $data = $app->db->select()->from('files')->orderBy('category', 'desc', 'modified')->execute()->asArray();
        } if(is_string($params['id'])) {
            $data = $app->db->select()->from('files')->where('category', $params['id'])->orderBy('category', 'desc', 'modified')->execute()->asArray();
        } else {
            Exception::throwHttpError(400);
        }

        foreach ($data as $file) {
            $json[$file->id]['desc'] = $file->desc;
            $json[$file->id]['category'] = $file->category;
            $json[$file->id]['modified'] = $file->modified;
        }

        echo json_encode($json);
    }

    public static function postIndex($params = null)
    {
        global $app;

        if (!\ZJX\Auth\Auth::inGroup(Groups::find("WM"))) { Exception::throwHttpError(400); }

        if ($params['id'] == "New") {

        }

        $data = Request::getInput();
        if (file_exists(Config::get('app.uploads' . $params['id']))) { Exception::throwHttpError(400); }

        $fp = fopen(Config::get('app.uploads') . $params['id'], "wb");
        fwrite($fp, $data);
        fclose($fp);
    }

    public static function checkAndRender($file, $download = false) {
        global $app;
        $of = $file;
        $file = Config::get('app.uploads') . $file;
        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        $filetype = $finfo->buffer(file_get_contents($file));
        if (!preg_match("/^[A-Z\-a-z0-9+=_!\.]+$/", $of)) { Exception::throwHttpError(400); }
        if (file_exists($file)) {
            while (@ob_end_clean()) { ; }

            header("Content-Type: " . $filetype);
            header("Content-Length: " . filesize($file));
            if ($download)
                header("Content-Disposition: attachment;");

            $handle = fopen($file, 'rb');
            if ($handle === false ) { return false; }

            while (!feof($handle))
            {
                print(fread($handle, 1 * 1024 * 1024));
                //echo $buf;
            }

            fclose($handle);
        } else {
            Exception::throwHttpError(404, $file . " does not exist.");
        }
    }
}
