</div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12"><p class="pull-left">Copyright &copy; 2014 by <a href="http://www.danielhawton.com">Daniel A. Hawton</a> and vZJX ARTCC.</p>
                <ul class="list-inline pull-right">
                    <li>For entertainment purposes only.  Do not use for real world purposes.  Part of the <a href="http://www.vatsim.net">VATSIM</a> Network.</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="pull-right">Powered by <a href="http://www.github.com/dhawton/Lyanna"><img src="http://www.danielhawton.com/assets/images/projects/Lyanna/lyanna-dark.jpg" style="height: 40px;"></a></p>
            </div>
        </div>
    </div>
</footer>
<?php
\Lyanna\View\Render::Scripts();
?>
</body>
</html>