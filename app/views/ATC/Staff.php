<?php
/****************************************************
 * Copyright 2014 Daniel A. Hawton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
global $data;

\Lyanna\View\View::showHeader();
?>
<div class="page-heading-two">
    <div class="container">
        <h2>ARTCC Staff</h2>
    </div>
</div>

<div class="container">
    <div class="about-us-two">
        <div class="team-two">
            <div class="block-heading-two">
                <h3><span>Air Traffic Manager</span></h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                <?php
                if (is_array($data['atm']) && count($data['atm']) > 0) {
                    foreach ($data['atm'] as $atm) {
                        genName($atm->firstname . " " . $atm->lastname);
                    }
                } else {
                    echo "<h4>Vacant</h4>";
                }
                ?>
                The Air Traffic Manager is responsible to the VATUSA Southern Region Director for the overall administration
                    of the ARTCC.  The ATM is responsible for appointing ARTCC staff members and delegation of authorities.
                </div>
            </div>
        </div>
        <div class="team-two">
            <div class="block-heading-two">
                <h3><span>Deputy Air Traffic Manager</span></h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (is_array($data['datm']) && count($data['datm']) > 0) {
                        foreach ($data['datm'] as $datm) {
                            genName($datm->firstname . " " . $datm->lastname);
                        }
                    } else {
                        echo "<h4>Vacant</h4>";
                    }
                    ?>
                    The Deputy Air Traffic Manager reports to the Air Traffic Manager and acts as Air Traffic Manager
                    in their absence.  The Deputy Air Traffic Manager is jointly responsible for administration and accuracy
                    of the roster including visiting controllers.
                </div>
            </div>
        </div>

        <div class="team-two">
            <div class="block-heading-two">
                <h3><span>Training Administrator</span></h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (is_array($data['ta']) && count($data['ta']) > 0) {
                        foreach ($data['ta'] as $ta) {
                            genName($ta->firstname . " " . $ta->lastname);
                        }
                    } else {
                        echo "<h4>Vacant</h4>";
                    }
                    ?>
                    The Training Administrator works with the Air Traffic Manager and Deputy Air Traffic Manager to build
                    training programmes, establish training procedures and recommend instructors and mentors.  The
                    Training Administrator works with Instructors and Mentors to develop knowledge and mentors to help
                    develop teaching ability.
                </div>
            </div>
        </div>

        <div class="team-two">
            <div class="block-heading-two">
                <h3><span>Training Administrator</span></h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (is_array($data['ata']) && count($data['ata']) > 0) {
                        foreach ($data['ata'] as $ta) {
                            genName($ta->firstname . " " . $ta->lastname);
                        }
                    } else {
                        echo "<h4>Vacant</h4>";
                    }
                    ?>
                    The Assistant Training Administrator is appointed by the Training Administrator and approved by
                    the Air Traffic Manager.  The Assistant Training Administrator assists the Training Administrator in
                    development and execution of training programmes, selection of Instructors and Mentors and other tasks
                    as directed.
                </div>
            </div>
        </div>

        <div class="team-two">
            <div class="block-heading-two">
                <h3><span>Events Coordinator</span></h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (is_array($data['ec']) && count($data['ec']) > 0) {
                        foreach ($data['ec'] as $ec) {
                            genName($ec->firstname . " " . $ec->lastname);
                        }
                    } else {
                        echo "<h4>Vacant</h4>";
                    }
                    ?>
                    The Events Coordinator is responsible to the Deputy Air Traffic Manager for the coordination, planning,
                    dissemination and creation of events to neighboring facilities, virtual airlines, VATUSA and VATSIM.
                </div>
            </div>
        </div>

        <div class="team-two">
            <div class="block-heading-two">
                <h3><span>Assistant Events Coordinator</span></h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (is_array($data['aec']) && count($data['aec']) > 0) {
                        foreach ($data['aec'] as $aec) {
                            genName($aec->firstname . " " . $aec->lastname);
                        }
                    } else {
                        echo "<h4>Vacant</h4>";
                    }
                    ?>
                    The Assistant Events Coordinator is responsible to the Events Coordination for assistance in
                    coordination, planning, dissemination and creation of events to neighboring facilities, virtual airlines,
                    VATUSA and VATSIM and other tasking as directed.
                </div>
            </div>
        </div>

        <div class="team-two">
            <div class="block-heading-two">
                <h3><span>Facility Engineer</span></h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (is_array($data['fe']) && count($data['fe']) > 0) {
                        foreach ($data['fe'] as $fe) {
                            genName($fe->firstname . " " . $fe->lastname);
                        }
                    } else {
                        echo "<h4>Vacant</h4>";
                    }
                    ?>
                    The Facility Engineer is responsible to the Senior Staff for creation of sector files, radar client files,
                    training scenarios, Letters of Agreement, Memorandums of Understanding, Standard Operating Procedures
                    and other requests as directed and submission to the Air Traffic Manager for approval prior to dissemination.
                </div>
            </div>
        </div>

        <div class="team-two">
            <div class="block-heading-two">
                <h3><span>Webmaster/IT Director</span></h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (is_array($data['wm']) && count($data['wm']) > 0) {
                        foreach ($data['wm'] as $wm) {
                            genName($wm->firstname . " " . $wm->lastname);
                        }
                    } else {
                        echo "<h4>Vacant</h4>";
                    }
                    ?>
                    Responsible to the Air Traffic Manager for the operation and maintenance of all IT services including, but not limited to,
                    the Website, Teamspeak and Email services and any other tasking as directed.
                </div>
            </div>
        </div>

        <div class="team-two">
            <div class="block-heading-two">
                <h3><span>Instructors</span></h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (is_array($data['ins']) && count($data['ins']) > 0) {
                        foreach ($data['ins'] as $ins) {
                            genName($ins->firstname . " " . $ins->lastname);
                        }
                    } else {
                        echo "<h4>Vacant</h4>";
                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="team-two">
            <div class="block-heading-two">
                <h3><span>Mentors</span></h3>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (is_array($data['mtr']) && count($data['mtr']) > 0) {
                        foreach ($data['mtr'] as $mtr) {
                            genName($mtr->firstname . " " . $mtr->lastname);
                        }
                    } else {
                        echo "<h4>Vacant</h4>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

function genName($name) {
    echo "<h4>$name <a href=\"#\"><i class=\"fa fa-envelope circle-3\"></i></a></h4>";
}
\Lyanna\View\View::showFooter();