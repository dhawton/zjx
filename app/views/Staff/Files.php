<?php
\Lyanna\View\View::showHeader();
?>
<div class="page-heading-two">
    <div class="container">
        <h2>Admin > Files</h2>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 text-right"><button class="btn btn-success zjx-file-new">New</button></div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class="table table-bordered text-center tablesorter" id="filetable">
                <thead>
                    <th>Title</th>
                    <th width="15%">Category</th>
                    <th width="15%">Modified</th>
                    <th width="20%">Actions</th>
                </thead>
                <tbody id="tableFiles"><td colspan="4"><img src="/assets/images/spinner_radar.gif"> Loading...</td></tbody>
            </table>
        </div>
    </div>
</div>
<?php
\Lyanna\View\Bundle::Scripts("files");

\Lyanna\View\View::showFooter();