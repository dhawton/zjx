<?php
/****************************************************
 * Copyright 2014 Daniel A. Hawton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ZJX\Auth;

require(__PUBLIC__ . "forum" . DIRECTORY_SEPARATOR . "SSI.php");

class Auth
{
    public static function inGroup($grp)
    {
        global $user_info;

        if (is_array($grp)) {
            foreach ($grp as $key) {
                if (in_array($grp[$key], $user_info['groups'])) return true;
            }
        } elseif (in_array($grp, $user_info['groups'])) {
            return true;
        }

        return false;
    }

    public static function User($cid)
    {
        global $app, $user_info;

        return $app->orm->get('Roster')->where('id', $user_info['username'])->find();
    }

    public static function check()
    {
        global $context;

        if (!$context['user']['is_guest'])
            return true;

        return false;
    }
} 