<?php
namespace ZJX\Forum;


class Groups {
    private static $groups = array(
        'ATM' => 9,
        'DATM' => 10,
        'TA' => 11,
        'ATA' => 12,
        'EC' => 13,
        'AEC' => 14,
        'FE' => 15,
        'WM' => 16,
        'INS' => 17,
        'MTR' => 18,
        'Controller' => 19
    );

    public static function find($grp) {
        if (static::$groups[$grp]) return static::$groups[$grp];
        foreach (static::$groups[$grp] as $ind => $val)
        {
            if ($val == $grp) return $ind;
        }

        return null;
    }

    function __get($var)
    {
        if (static::$groups[$var]) return static::$groups[$var];
        foreach (static::$groups[$var] as $ind => $val)
        {
            if ($val == $var) return $ind;
        }

        return null;
    }
} 