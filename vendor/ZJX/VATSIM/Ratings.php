<?php
/****************************************************
 * Copyright 2014 Daniel A. Hawton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ZJX\VATSIM;


class Ratings
{
    protected static $ratings = array(
        0 => 'OBS',
        1 => 'S1',
        2 => 'S2',
        3 => 'S3',
        4 => 'C1',
        5 => 'C2',
        6 => 'C3',
        7 => 'I1',
        8 => 'I2',
        9 => 'I3',
        10 => 'SUP',
        11 => 'ADM'
    );
    protected static $long = array(
        0 => 'Observer',
        1 => 'Student 1',
        2 => 'Student 2',
        3 => 'Student 3',
        4 => 'Controller',
        5 => 'Controller 2',
        6 => 'Senior Controller',
        7 => 'Instructor',
        8 => 'Instructor 2',
        9 => 'Senior Instructor',
        10 => 'Supervisor',
        11 => 'Administrator'
    );
    public static function lookup($key)
    {
        if (is_int($key))
            if (isset(self::$ratings[$key]))
                return self::$ratings[$key];
            else
                return null;
        else {
            $key = array_search($key, self::$ratings);
            if ($key) { return $key; }
        }
        return null;
    }
    public static function lookupLong($key)
    {
        if (is_int($key))
            if (isset(self::$long[$key]))
                return self::$long[$key];
            else
                return null;
        else {
            $key = array_search($key, self::$long);
            if ($key) { return $key; }
        }
        return null;
    }
} 