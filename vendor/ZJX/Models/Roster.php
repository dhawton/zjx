<?php
/****************************************************
 * Copyright 2014 Daniel A. Hawton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace ZJX\Model;

class Roster
    extends \Lyanna\ORM\Model
{
    public $table = "roster";

    function __get($var)
    {
        global $app;

        if ($var == "email") {
            $email = $app->db->query('select')->table('smf_members')->fields('email_address')
                ->where('member_name', $this->id)->limit(1)
                ->execute();
            return $email->email_address;
        }

        parent::__get($var);
    }

    function __set($var, $val)
    {
        global $app;

        if ($var == "email") {
            return $app->db->query('update')->table('smf_members')
                ->data(array('email_address', $val))
                ->where('member_name', $this->id)->limit(1)
                ->execute();
        }

        parent::__set($var, $val);
    }
} 